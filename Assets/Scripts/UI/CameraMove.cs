﻿using UnityEngine;

namespace Hexiv {
	[RequireComponent(typeof(Camera))]
	public class CameraMove : MonoBehaviour {
		private Camera m_Camera;

		private void Awake() {
			m_Camera = GetComponent<Camera>();
		}

		private void Update() {
			transform.position += new Vector3(
				Input.GetAxis("LookHorizontal"),
				Input.GetAxis("LookVertical"),
				0
			);
			// TODO limit camera position
			m_Camera.orthographicSize *= Input.GetAxis("Zoom") + 1;
		}
	}
}
