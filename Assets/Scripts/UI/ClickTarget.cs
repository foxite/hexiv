﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hexiv {
	public interface IClickable {
		/// <summary>
		/// Called by GridCursor when this object is clicked.
		/// </summary>
		void Click();
	}
}
