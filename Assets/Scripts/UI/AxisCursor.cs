﻿using UnityEngine;

namespace Hexiv {
	[RequireComponent(typeof(GridCursor))]
	public class AxisCursor : MonoBehaviour {
		private GridCursor m_GC;

		private void Awake() {
			if (Input.mousePresent) {
				Cursor.visible = false;
				//Cursor.lockState = CursorLockMode.Locked;
				m_GC = GetComponent<GridCursor>();
			} else {
				Debug.LogWarning("A MouseCursor script was instantiated but there is no mouse present.");
				Destroy(this);
			}
		}

		private void Update() {
			/*m_GC.MoveCursor(new Vector2(
				Input.GetAxis("Mouse X"),
				Input.GetAxis("Mouse Y")
			));*/
			m_GC.SetCursor(Input.mousePosition);
		}
	}
}
