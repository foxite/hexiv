﻿using UnityEngine;

namespace Hexiv {
	public class GridCursor : Singleton<GridCursor> {
		private RectTransform m_RectTransform;
		private Camera m_Camera;

		private void Start() {
			m_RectTransform = transform as RectTransform;
			if (m_RectTransform is null) {
				Debug.LogError("GridCursor requires a RectTransform.", this);
				Destroy(this);
			}
			m_Camera = Camera.main;
		}

		public void SetCursor(Vector2 newPosition) {
			m_RectTransform.anchoredPosition = newPosition / new Vector2(m_RectTransform.lossyScale.x, m_RectTransform.lossyScale.y);
			EnsureCursorBounds();
		}

		public void MoveCursor(Vector2 delta) {
			m_RectTransform.anchoredPosition += delta / new Vector2(m_RectTransform.lossyScale.x, m_RectTransform.lossyScale.y);
			EnsureCursorBounds();
		}

		private void Update() {
			if (Input.GetButtonDown("Click")) {
				var parent = m_RectTransform.parent as RectTransform;
				Vector2 viewportPosition = m_RectTransform.anchoredPosition / parent.sizeDelta;
				Ray ray = m_Camera.ViewportPointToRay(viewportPosition);
				if (Physics.Raycast(ray, out RaycastHit hit)) {
					if (hit.transform.TryGetComponent(out IClickable clickable)) {
						clickable.Click();
					}
				}
			}
		}

		private void EnsureCursorBounds() {
			Rect rect = (m_RectTransform.parent as RectTransform).rect;
			rect.x = 0;
			rect.y = 0;
			m_RectTransform.anchoredPosition = m_RectTransform.anchoredPosition.Clamp(rect);
		}
	}
}
