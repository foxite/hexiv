﻿using UnityEngine;

namespace Hexiv {
	public class GridSetup : MonoBehaviour {
		[SerializeField] private int m_Width, m_Height;

		public Vector2Int Size { get; set; }
		
		private void Start() {
			if (Size.x == 0 || Size.y == 0) {
				// Use inspector values if property was not set at runtime
				Size = new Vector2Int(m_Width, m_Height);
			}

			HexGrid grid = GetComponent<HexGrid>();
			for (int x = 0; x < Size.x; x++) {
				for (int y = 0; y < Size.y; y++) {
					grid.AddHex(HexUtil.EvenqToCube(new Vector2Int(x, y)));
				}
			}
			Destroy(this);
		}
	}
}
