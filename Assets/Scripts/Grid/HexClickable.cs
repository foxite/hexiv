﻿using UnityEngine;

namespace Hexiv {
	public class HexClickable : MonoBehaviour, IClickable {
		private Hex m_Hex;

		private void Awake() {
			m_Hex = transform.parent.GetComponent<Hex>();
		}

		public void Click() {
			HexSelection.Instance.SelectedHex = m_Hex;
		}
	}
}
