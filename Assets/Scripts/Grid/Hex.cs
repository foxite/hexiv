﻿using UnityEngine;

namespace Hexiv {
	public class Hex : MonoBehaviour {
		// Use materials, rather than colors, so we can have fancy shader stuff if we want
		[SerializeField] private Material m_SelectMaterial, m_NormalMaterial;

		private LineRenderer m_LineRenderer;
		private bool m_Started = false;
		private ActiveUnit m_Unit;

		public HexGrid Grid { get; private set; }
		public Vector3Int CubePosition { get; internal set; }
		public ActiveUnit Unit {
			get => m_Unit;
			set {
				if (m_Unit != null && value != null) {
					Debug.LogError("Tried to assign a unit to a hex that already had a unit!", this);
				} else {
					m_Unit = value;
				}
			}
		}

		private void Awake() {
			m_LineRenderer = GetComponent<LineRenderer>();
			Grid = HexGrid.Instance;
		}

		private void Start() {
			m_Started = true;
			Grid.AddHex(this);
		}

		public void OnSelect() {
			m_LineRenderer.material = m_SelectMaterial;
			transform.Translate(0, 0, -1); // Make highlighted border appear in front of the neighbors' borders
		}

		public void OnDeselect() {
			m_LineRenderer.material = m_NormalMaterial;
			transform.Translate(0, 0, 1); // Revert movement from OnSelect
		}

		private void OnEnable() {
			// OnEnable is called right after Awake and before Instantiate() returns, so this should not be done if CubePosition has not been set by HexGrid
			if (m_Started) {
				Grid.AddHex(this);
			}
		}

		private void OnDisable() {
			if (m_Started) {
				Grid.RemoveHex(this);
			}
		}
	}
}
