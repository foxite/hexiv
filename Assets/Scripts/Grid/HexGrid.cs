﻿using System.Collections.Generic;
using UnityEngine;

namespace Hexiv {
	/// <summary>
	/// This class allows you to manipulate hexes using cube coordinates.
	/// </summary>
	/// <remarks>
	/// Everything about hex coordinates is explained here: https://www.redblobgames.com/grids/hexagons/
	/// We use cube coordinates to do operations on the grid. Odd-r coordinates are used when placing hexes in the Unity scene.
	/// Use <see cref="HexUtil"/> to convert cube to odd-r coordinates, although in theory you should never have to do so unless you're doing the above.
	/// </remarks>
	public class HexGrid : Singleton<HexGrid> {
		[SerializeField] private Hex m_HexPrefab;

		private Dictionary<Vector2Int, Hex> m_CubeGrid;

		protected override void Awake() {
			base.Awake();

			// This must be done in Awake because GridSetup runs the AddHex function in Start.
			// And simply to follow the convention where all private fields are initialized in Awake, and not actually used until Start.
			// Doing so prevents 99% of NREs when instantiating objects because Awake runs immediately after instantiation, before Instantiate() returns, but Start won't run until the frame after that.
			m_CubeGrid = new Dictionary<Vector2Int, Hex>();
		}

		/// <summary>
		/// Adds a hex to the grid, and creates its GameObject in the unity scene.
		/// </summary>
		/// <param name="cubePosition"></param>
		public void AddHex(Vector3Int cubePosition) {
			Vector2Int evenq = HexUtil.CubeToEvenq(cubePosition);

			var scenePosition = new Vector3(
				3f / 2f * evenq.x * 0.6f,
				Mathf.Sqrt(3) * (evenq.y - 0.5f * (evenq.x & 1)) * 0.57735f
			);

			Hex newHex = Instantiate(m_HexPrefab, scenePosition, m_HexPrefab.transform.rotation, transform);
			newHex.name = cubePosition.ToString() + "; " + evenq.ToString();
			newHex.CubePosition = cubePosition;
			// newHex will call AddHex with itself in its OnEnable
		}

		public void AddHex(Hex hex) {
			m_CubeGrid.Add(HexUtil.CubeToEvenq(hex.CubePosition), hex);
		}

		public void RemoveHex(Hex hex) => RemoveHex(hex.CubePosition);
		public void RemoveHex(Vector3Int cubePosition) {
			m_CubeGrid.Remove(HexUtil.CubeToEvenq(cubePosition));
		}

		public Hex GetHex(Vector3Int cubePosition) => m_CubeGrid[HexUtil.CubeToEvenq(cubePosition)];
		public bool TryGetHex(Vector3Int cubePosition, out Hex hex) => m_CubeGrid.TryGetValue(HexUtil.CubeToEvenq(cubePosition), out hex);
	}
}
