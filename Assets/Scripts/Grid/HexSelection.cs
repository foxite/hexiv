﻿using System;

namespace Hexiv {
	// This class simply holds the currently selected hex and fires an event when it changes.
	public class HexSelection : Singleton<HexSelection> {
		private Hex m_SelectedHex;

		public event EventHandler<ValueChangedEvent<Hex>> SelectionChanged;

		public Hex SelectedHex { 
			get => m_SelectedHex;
			set {
				SelectionChanged?.Invoke(this, new ValueChangedEvent<Hex>(m_SelectedHex, value));

				// This could be done through the event, by subscribing every Hex to it, but then we would be running potentially hundreds of event handlers when we know exactly
				//  which objects actually care about the event, which are two at most.
				if (m_SelectedHex != null) {
					m_SelectedHex.OnDeselect();
				}
				m_SelectedHex = value;

				if (m_SelectedHex != null) {
					m_SelectedHex.OnSelect();
				}
			}
		}
	}
}
