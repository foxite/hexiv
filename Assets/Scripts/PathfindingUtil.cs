﻿using System;
using System.Collections.Generic;
using System.Linq;
using Priority_Queue;

namespace Hexiv {
	public static class PathfindingUtil {
		/// <summary>
		/// Finds a path between two <see cref="PathfindingCell"/>s.
		/// </summary>
		/// <returns>
		/// If start == end: An empty enumeration
		/// If no path is possible: An single null item
		/// Otherwise: A lazy enumeration with the steps in the path, including <paramref name="start"/> end <paramref name="end"/>.
		/// </returns>
		public static IEnumerable<Cell> FindPath<Cell>(Cell start, Cell end) where Cell : PathfindingCell {
			if (start == end) {
				yield break;
			}

			// The PriorityQueue library has a FastPriorityQueue that has tons of optimizations but there's a big problem: you need to define the max size and it doesn't resize itself.
			// SimplePriorityQueue isn't as fast, but still pretty fast and it has no maximum size.
			var frontier = new SimplePriorityQueue<Cell>();
			var cameFrom = new Dictionary<Cell, Cell>();
			var costSoFar = new Dictionary<Cell, int>();

			// We start searching at the end and look for the start.
			// We do it this way because when we create a path, it will be backwards.
			// So if we do this backwards to begin with, then the returned list will actually be forwards.
			frontier.Enqueue(end, 0);
			costSoFar[end] = end.Cost;
			
			// This is a local function so we can break out of the nested loop more easily
			bool traverse() {
				while (frontier.TryDequeue(out Cell current)) {
					foreach (Cell next in current.Neighbors.Except(cameFrom.Keys)) {
						int newCost = costSoFar[current] + next.Cost;
						if (!costSoFar.TryGetValue(next, out int cost) || newCost < cost) {
							cameFrom[next] = current;

							if (next == start) {
								return true;
							}

							costSoFar[next] = newCost;
							frontier.Enqueue(next, newCost + end.Heuristic(next));
						}
					}
				}
				return false;
			}

			if (traverse()) {
				Cell current = start;
				int i = 0;
				while (current != end) {
					current = cameFrom[current];
					i++;
					yield return current;
				}
			} else {
				yield return null;
			}
		}
	}

	public abstract class PathfindingCell : IEquatable<PathfindingCell> {
		public abstract int Cost { get; }

		/// <summary>
		/// Cells that can be reached directly when standing on this cell. The Cost of an enumerated PathfindingCell is the cost of moving from this cell to that cell.
		/// It is highly recommended that you use lazy evaluation for this, as doing so eliminates all wasted effort from preparing a list of cells if not all of them will be visited.
		/// </summary>
		public abstract IEnumerable<PathfindingCell> Neighbors { get; }

		/// <summary>
		/// Return the distance to another PathfindingCell.
		/// </summary>
		public abstract int Heuristic(PathfindingCell pfc);

		public abstract bool Equals(PathfindingCell other);
		public abstract override int GetHashCode();

		public override bool Equals(object obj) => obj is PathfindingCell pfc && Equals(pfc);

		public static bool operator ==(PathfindingCell left, PathfindingCell right) => left?.Equals(right) ?? right is null;
		public static bool operator !=(PathfindingCell left, PathfindingCell right) => !(left == right);
	}

	public class HexPathfindingCell : PathfindingCell {
		public Hex TheHex { get; }

		public override int Cost => 1;

		// Thanks to LINQ this is lazily evaluated. If we happen to find a path after only a few steps, then we won't have wasted a bunch of time preparing a list of cells to visit.
		public override IEnumerable<PathfindingCell> Neighbors =>
			from pos in HexUtil.CubeSixDirections(TheHex.CubePosition)
			let hex = TheHex.Grid.TryGetHex(pos, out Hex ret) ? ret : null
			where hex != null
			select new HexPathfindingCell(hex);

		public HexPathfindingCell(Hex hex) {
			TheHex = hex;
		}

		public override int Heuristic(PathfindingCell pfc) => pfc is HexPathfindingCell hpc
			? HexUtil.CubeDistance(hpc.TheHex.CubePosition, hpc.TheHex.CubePosition)
			: throw new ArgumentException("Received a PFC that does not derive from HexPathfindingCell");

		public override bool Equals(PathfindingCell other) => other is HexPathfindingCell hpc && TheHex.CubePosition.Equals(hpc.TheHex.CubePosition);
		public override int GetHashCode() => TheHex.CubePosition.GetHashCode();
	}
}
