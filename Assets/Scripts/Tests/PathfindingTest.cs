﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Hexiv {
	public class PathfindingTest : MonoBehaviour {
		private UIPhase m_Phase = UIPhase.SelectingStart;
		private Hex m_Start;
		private IEnumerable<HexPathfindingCell> m_Path;
		private bool m_SpaceDown;

		private void Start() {
			HexSelection.Instance.SelectionChanged += OnSelectionChanged;
		}

		private void OnSelectionChanged(object sender, ValueChangedEvent<Hex> e) {
			if (m_Phase == UIPhase.SelectingStart) {
				m_Start = e.newValue;
				Debug.Log("Selected start, now select the end");
				m_Phase = UIPhase.SelectingEnd;
			} else if (m_Phase == UIPhase.SelectingEnd) {
				m_Path = PathfindingUtil.FindPath(new HexPathfindingCell(m_Start), new HexPathfindingCell(e.newValue));
				if (!m_Path.Any()) {
					Debug.Log("Start is end");
				} else if (m_Path.First() == null) {
					Debug.Log("No path");
				} else {
					StartCoroutine(DisplayPathProgress());
				}
			}
		}

		private void Update() {
			if (m_Phase == UIPhase.Displaying && Input.GetKeyDown(KeyCode.Space)) {
				m_SpaceDown = true;
			}
		}

		private IEnumerator DisplayPathProgress() {
			m_Phase = UIPhase.Displaying;
			yield return null;

			foreach (HexPathfindingCell cell in m_Path) {
				HexSelection.Instance.SelectedHex = cell.TheHex;

				while (!m_SpaceDown) {
					yield return null;
				}
				m_SpaceDown = false;
			}
			m_Phase = UIPhase.SelectingStart;
		}

		private enum UIPhase {
			SelectingStart,
			SelectingEnd,
			Displaying
		}
	}
}
