﻿using UnityEngine;

namespace Hexiv {
	public class HighlightNeighbors : MonoBehaviour {
		private void Start() {
			HexSelection.Instance.SelectionChanged += Instance_SelectionChanged;
		}

		private void Instance_SelectionChanged(object sender, ValueChangedEvent<Hex> e) {
			foreach (HexPathfindingCell cell in new HexPathfindingCell(e.newValue).Neighbors) {
				cell.TheHex.transform.Rotate(0, 20, 0);
			}
		}
	}
}
