﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hexiv {
	public class UnitAction : ScriptableObject {
		[SerializeField] private int m_AP;

		public int AP => m_AP;
	}
}
