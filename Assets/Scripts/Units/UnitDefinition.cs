﻿using UnityEngine;

namespace Hexiv {
	public class UnitDefinition : ScriptableObject {
		[SerializeField] private int m_MaxHealth;
		[SerializeField] private int m_Power;
		[SerializeField] private int m_MaxAP;

		public int MaxHealth => m_MaxHealth;
		public int Power => m_Power;
		public int MaxAP => m_MaxAP;
	}
}
