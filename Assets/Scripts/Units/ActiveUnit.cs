﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Hexiv {
	public class ActiveUnit : MonoBehaviour {
		[SerializeField] private UnitDefinition m_InitialUnit;
		private int m_Health;
		private Hex m_CurrentHex;

		public UnitDefinition Definition { get; private set; }
		public Hex CurrentHex {
			get => m_CurrentHex;
			private set {
				if (m_CurrentHex != value) {
					if (m_CurrentHex.Unit == this) {
						m_CurrentHex.Unit = null;
					}

					m_CurrentHex = value;
					m_CurrentHex.Unit = this;
				}
			}
		}

		public int Health {
			get => m_Health;
			set {
				m_Health = value;
				if (m_Health > Definition.MaxHealth) {
					m_Health = Definition.MaxHealth;
				} else if (m_Health < 0) {
					Die();
				}
			}
		}

		public int AP { get; private set; }

		private void Awake() {
			Definition = m_InitialUnit;
		}

		protected virtual void UpdateTurn() { }

		public void SetDefinition(UnitDefinition definition) {
			if (Definition == null) {
				Definition = definition;
			} else {
				Debug.LogError("A unit definition has already been assigned", this);
			}
		}

		protected void Die() {
			Debug.Log("Unit died", this);
		}

		public bool CanPerformAction(UnitAction action) => action.AP <= AP;

		protected void Move(Hex toHex) {
			if (toHex.Unit == null) {
				CurrentHex = toHex;
			}
		}

		protected bool PerformAction(UnitAction action) {
			if (AP >= action.AP) {
				AP -= action.AP;
				// TODO execute action
				return true;
			} else {
				return false;
			}
		}
	}
}
